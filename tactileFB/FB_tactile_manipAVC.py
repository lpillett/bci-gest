#!/usr/bin/python

# ------------------------------------------------------------------------------
# Author : camille jeunet & lea pillette
# Date : 11-04-2017
# Purpose : XP stroke - tactile & visual feedback
# Run Arduino program motor.ino -- tactile feedback (e.g., positive only, biased)
# ------------------------------------------------------------------------------

# add pylsl to known path
from math import copysign
import sys; sys.path.append('./pylsl') 
from pylsl import StreamInlet, resolve_stream

import serial
import time
import random
import math
   
# Windows
serialPort = serial.Serial('COM14', 9600, timeout=1)

# enter the participant ID and the run number
S = input('Please enter the ID of the participant: ')
#r = input('Please enter the run number: ')

# ------------------------------------------------------------------------------
# GET THE PARAMETERS FROM THE CONFIG FILE
# ------------------------------------------------------------------------------

# opens the configuration file
file = open(('openvibe_xp_files_and_signals/signals/configS' + str(S) + '.txt'), 'r')
lines = file.readlines()

# positive only: Y (for yes) or N (for no) - if Y, no negative feedback
positive_only = lines[0][-2]

# positive_bias: if Y, the feedback is switched of "strength_bias" in the correct direction 
positive_bias = lines[2][-2]
strength_bias = lines[3][-4:-1]

# to get the min and max intensities on the left hand
# and the smallest perceivable difference of intensities on the left hand
MINintensity_left = float(lines[5].split(': ')[1][0:len(lines[5].split(': ')[1])-1])
MAXintensity_left = float(lines[6].split(': ')[1][0:len(lines[6].split(': ')[1])-1])
nb_thresholds_left = int(float(lines[10].split(': ')[1][0:len(lines[10].split(': ')[1])-1]))
median_left = float(lines[21].split(': ')[1][0:len(lines[21].split(': ')[1])-1])
MAD_left = float(lines[22].split(': ')[1][0:len(lines[22].split(': ')[1])-1])

min_visu_left = float(lines[13].split(': ')[1][0:len(lines[13].split(': ')[1])-1])
max_visu_left = float(lines[14].split(': ')[1][0:len(lines[14].split(': ')[1])-1])
#print (MAD_left)

# to get the min and max intensities on the right hand
# and the smallest perceivable difference of intensities on the right hand
MINintensity_right = float(lines[7].split(': ')[1][0:len(lines[7].split(': ')[1])-1])
MAXintensity_right = float(lines[8].split(': ')[1][0:len(lines[8].split(': ')[1])-1])
nb_thresholds_right = int(float(lines[11].split(': ')[1][0:len(lines[11].split(': ')[1])-1]))
median_right = float(lines[23].split(': ')[1][0:len(lines[23].split(': ')[1])-1])
MAD_right = float(lines[24].split(': ')[1][0:len(lines[24].split(': ')[1])-1])
min_visu_right = float(lines[15].split(': ')[1][0:len(lines[15].split(': ')[1])-1])
max_visu_right = float(lines[16].split(': ')[1][0:len(lines[16].split(': ')[1])-1])
#print (MAD_right)

file.close()


# ------------------------------------------------------------------------------
# SET THE PARAMETERS FOR THE SESSION
# ------------------------------------------------------------------------------

#file = open(('openvibe_xp_files_and_signals/signals/configS' + str(S) + 'r' + str(r) + '.txt'),'a')

# set the normal number of thresholds between the min and max intensities
threshold_left = int((MAXintensity_left - MINintensity_left) / nb_thresholds_left)
threshold_right = int((MAXintensity_right - MINintensity_right) / nb_thresholds_right)

# create a list containing the values of the classifier output for which the
# intensity will change on the left hand
change_intensity_left = (max_visu_left - min_visu_left) / nb_thresholds_left
List_thresholds_left = [0]*(nb_thresholds_left + 1)
for i in range(len(List_thresholds_left)):
   List_thresholds_left[i] = min_visu_left + i * change_intensity_left

# create a list containing the values of the classifier output for which the
# intensity will change on the right hand	
change_intensity_right = (max_visu_right - min_visu_right) / nb_thresholds_right
List_thresholds_right = [0]*(nb_thresholds_right +1)
for i in range (len(List_thresholds_right)):
   List_thresholds_right[i] = min_visu_right + i * change_intensity_right

# write the threshold value & number for each hand in the config file
#file.write('\r\n threshold_left_tact: ' + str(threshold_left))
#file.write('\r\n nb_thresholds_left_tact: ' + str(nb_thresholds_left))
#file.write('\r\n threshold_right_tact: ' + str(threshold_right))
#file.write('\r\n nb_thresholds_right_tact: ' + str(nb_thresholds_right))

#file.close()


# ------------------------------------------------------------------------------
# CONNECTION TO ARDUINO
# ------------------------------------------------------------------------------

# wait for Arduino setup to finish
time.sleep(2)
connected = False
print 'Waiting for Arduino...'
while not connected:
   serin = serialPort.readline()
   if serin == 'running motor.ino\r\n':
      print 'A: ' + serin
      connected = True
   else:
      print 'A: ' + serin

# wait for motors to wake up (a small mistake in the motor shield :-)
time.sleep(2);
   

# ------------------------------------------------------------------------------
# COMMUNICATION WITH OPENVIBE
# ------------------------------------------------------------------------------

# first resolve an EEG stream on the lab network
print("Looking for the stream...")
streams = resolve_stream('type', 'XPstroke_FBtact')
# create a new inlet to read from the stream
inlet = StreamInlet(streams[0])
print("Stream found")


# ------------------------------------------------------------------------------
# ACTION
# ------------------------------------------------------------------------------
   
display_fb = False
stim = 0
instruction = 0
performance_left = 0
performance_right = 0
motors_left = [0, 1] # id of the motor to be activated (can be modified)
motors_right = [4, 5] # id of the motor to be activated (can be modified)

while True:
        
   # get a new sample (you can also omit the timestamp part if you're not
   # interested in it)
   sample, timestamp = inlet.pull_sample()
   if sample[4] != 0:
      stim = sample[4]
      print(stim)

   # to display the current intruction
   if stim == 769 and sample[4]!= 0:
      instruction = 769
   elif stim == 770 and sample[4]!= 0:
      instruction = 770
   elif stim == 780 and sample[4]!= 0:
      instruction = 780
      
   # to stop the motors at the end of a trial
   elif stim == 800:
      serialPort.write('a\r\n')
      display_fb = False
   
   # to send the command to the Arduino
   elif stim == 781 or display_fb:
      display_fb = True
      #serialPort.write('a\r\n')
      print("PerfLeft : " + str(performance_left) + "   PerfRight : " + str(performance_right))
      
      performance_left = (((abs(sample[1])-abs(sample[0]))/(abs(sample[1])+abs(sample[0]))) - median_left) / (MAD_left*2);
      performance_right = (((abs(sample[3])-abs(sample[2]))/(abs(sample[3])+abs(sample[2]))) - median_right) / (MAD_right*2);

      # No Positive Biais - determine the intensity on the right and left hand depending on the parameters & on
      # the classifier output
##      if positive_bias == 'N':
      intensity_left = MINintensity_left + threshold_left * List_thresholds_left.index(min(List_thresholds_left, key=lambda x:abs(x-abs(performance_left))))
      intensity_right = MINintensity_right + threshold_right * List_thresholds_right.index(min(List_thresholds_right, key=lambda x:abs(x-abs(performance_right))))
         
      # Positive Biais - if we are in the positive bias condition, we transform the output as a
      # function of the strength of the bias and then send the appropriate fb
##      else:
##         new_sample = sample[0] + copysign(float(strength_bias),(instruction-769.5))
##         intensity_left = MINintensity_left + threshold_left * List_thresholds_left.index(min(List_thresholds_left, key=lambda x:abs(x-abs(new_sample))))
##         intensity_right = MINintensity_right + threshold_right * List_thresholds_right.index(min(List_thresholds_right, key=lambda x:abs(x-abs(new_sample))))

      # if we are in the positive only condition, provide feedback only when the
      # classifier output corresponds to the instruction
      if positive_only == 'Y':
         if instruction == 769 and performance_left >= 0 : #left
            print("index : " + str(List_thresholds_left.index(min(List_thresholds_left, key=lambda x:abs(x-abs(performance_left))))))
            serialPort.write('i' + str(intensity_left) + '\r\n')
            for motor_left in motors_left:
               serialPort.write('m' + str(motor_left) + '\r\n')
         elif instruction == 770 and performance_right >= 0 : #right
            print("index : " + str(List_thresholds_right.index(min(List_thresholds_right, key=lambda x:abs(x-abs(performance_right))))))
            serialPort.write('i' + str(intensity_right) + '\r\n')
            for motor_right in motors_right:
               serialPort.write('m' + str(motor_right) + '\r\n')
         elif (instruction == 769 and performance_left <= 0) or (instruction == 770 and performance_right <= 0):
            serialPort.write('a\r\n')

      
      # otherwise we send a not positive only FB (but it can be biased)
      else :
         if sample[0] < 0: #left hand recognised
            serialPort.write('i' + str(intensity_left) + '\r\n')
            for motor_left in motors_left:
               serialPort.write('m' + str(motor_left) + '\r\n')
         else: #right hand recognised
            serialPort.write('i' + str(intensity_right) + '\r\n')
            for motor_right in motors_right:
               serialPort.write('m' + str(motor_right) + '\r\n')

   
      

      
   
   

    
    
    
