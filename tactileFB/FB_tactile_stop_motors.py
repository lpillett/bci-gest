#!/usr/bin/python

# ------------------------------------------------------------------------------
# Author : camille jeunet
# Date : 11-04-2017
# Purpose : XP stroke - tactile & visual feedback
# Run Arduino program motor.ino -- tactile feedback (e.g., positive only, biased)
# ------------------------------------------------------------------------------

# add pylsl to known path
from math import copysign
import sys; sys.path.append('./pylsl') 
from pylsl import StreamInlet, resolve_stream

import serial
import time
import random
   
# Windows
serialPort = serial.Serial('COM14', 9600, timeout=1)

# ------------------------------------------------------------------------------
# CONNECTION TO ARDUINO
# ------------------------------------------------------------------------------

# wait for Arduino setup to finish
time.sleep(2)
connected = False
print('Waiting for Arduino...')
while not connected:
   serin = serialPort.readline()
   if serin == 'running motor.ino\r\n':
      print('A: ' + serin)
      connected = True
   else:
      print('A: ' + serin)

# wait for motors to wake up (a small mistake in the motor shield :-)
time.sleep(2);

serialPort.write('a\r\n')
