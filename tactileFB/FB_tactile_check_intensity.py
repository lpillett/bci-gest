#!/usr/bin/python

# ------------------------------------------------------------------------------
# Authors : camille jeunet & lea pillette
# Date : 11-04-2017
# Purpose : XP stroke - tactile & visual feedback
# Run Arduino program motor.ino -- tactile feedback (e.g., positive only, biased)
# ------------------------------------------------------------------------------

# add pylsl to known path
from math import copysign
import sys; sys.path.append('./pylsl') 
from pylsl import StreamInlet, resolve_stream

import serial
import time
import random

from random import shuffle
   
# Windows
serialPort = serial.Serial('COM14', 9600, timeout=1)

# enter the participant ID and the run number
S = input('Please enter the ID of the participant: ')
#r = input('Please enter the session number: ')

# ------------------------------------------------------------------------------
# GET THE DEFAULT PARAMETERS
# ------------------------------------------------------------------------------

file_loc_name = ('openvibe_xp_files_and_signals/signals/configS' + str(S) + '.txt')

motors_left = [0, 1] # id of the motor to be activated (can be modified)
motors_right = [4, 5] # id of the motor to be activated (can be modified)

nb_steps4computation = 20 # nb of steps to find the min & max (can be modified)

MINintensity_left = 50 # random value - to be adjusted
MAXintensity_left = 200 # random value - to be adjusted
nb_thresholds_left = 5

step_left = int((float(MAXintensity_left) - float(MINintensity_left))/float(nb_steps4computation))
threshold_left = int((float(MAXintensity_left) - float(MINintensity_left))/float(nb_thresholds_left))

MINintensity_right = 50 # random value - to be adjusted
MAXintensity_right = 200 # random value - to be adjusted
nb_thresholds_right = 5

step_right = int((float(MAXintensity_right) - float(MINintensity_right))/float(nb_steps4computation))
threshold_right = int((float(MAXintensity_right) - float(MINintensity_right))/float(nb_thresholds_right))

# ------------------------------------------------------------------------------
# CONNECTION TO ARDUINO
# ------------------------------------------------------------------------------

# wait for Arduino setup to finish
time.sleep(2)
connected = False
print 'Waiting for Arduino...'
while not connected:
   serin = serialPort.readline()
   if serin == 'running motor.ino\r\n':
      print 'A: ' + serin
      connected = True
   else:
      print 'A: ' + serin

# wait for motors to wake up (a small mistake in the motor shield :-)
time.sleep(2);

   
# ------------------------------------------------------------------------------
# STEP 1 - DETERMINE MIN & MAX INTENSITIES
#           decision = 1 "too low intensity" (not perceivable)
#           decision = 2 "ok" (perceivable & not painful)
#           decision = 3 "too high intensity" (painful)
# ------------------------------------------------------------------------------

# STEP 1.1 - MIN intensity LEFT

print '--- Finding the MIN intensity for the LEFT hand ---'
decision = '0'
min_intens_left = MINintensity_left

while decision != '2':
   serialPort.write('i' + str(min_intens_left) + '\r\n')
   for motor_left in motors_left:
      serialPort.write('m' + str(motor_left) + '\r\n')
   decision = raw_input('Please enter the decision (1 for "too low", 2 for "ok" or 3 for "too high" or 4 to take a break): ')
   serialPort.write('a\r\n')
   if decision == '1' and min_intens_left != MAXintensity_left:
      min_intens_left = min_intens_left + step_left
   elif decision == '1' and min_intens_left == MAXintensity_left:
      print "It seems that the MAX intensity is too low to be perceived"
      break
   elif decision == '3' and min_intens_left == MINintensity_left:
      print "The experimenter should redefine the MIN intensity for the LEFT hand as it is already too strong"
      break
   elif decision == '3' and min_intens_left != MINintensity_left:
      decision = raw_input('Are you sure? The previous one was too low and this one too high - Please enter the decision again (should be 1 for "too low" or 2 for "ok"): ')
   elif decision == '4':
      startagain = raw_input('Enter something whenever you are ready to start again')
   elif decision != '1' and decision != '2' and decision != '3':
      print 'Please enter a correct value between the ones proposed'

# STEP 1.2 - MAX intensity LEFT

print '--- Finding the MAX intensity for the LEFT hand ---'
decision = '0'
max_intens_left = MAXintensity_left

while decision != '2':
   serialPort.write('i' + str(max_intens_left) + '\r\n')
   for motor_left in motors_left:
      serialPort.write('m' + str(motor_left) + '\r\n')
   decision = raw_input('Please enter the decision (1 for "too low", 2 for "ok" or 3 for "too high" or 4 to take a break): ')
   serialPort.write('a\r\n')
   if decision == '3' and max_intens_left != MINintensity_left:
      max_intens_left = max_intens_left - step_left
   elif decision == '3' and max_intens_left == MINintensity_left:
      print "It seems that the MIN intensity is too high and painful - to be adjusted"
      break
   elif decision == '1' and max_intens_left == MAXintensity_left:
      print "The experimenter should redefine the MAX intensity for the LEFT hand as it is not perceivable (or the patient has no sensations?)"
      break
   elif decision == '1' and max_intens_left != MAXintensity_left:
      decision = raw_input('Are you sure? The previous one was too high and this one too low - Please enter the decision again (should be 2 for "ok" or 3 for "too high"): ')
   elif decision != '1' and decision != '2' and decision != '3':
      print 'Please enter a correct value between the ones proposed'

# STEP 1.3 - MIN intensity RIGHT

print '--- Finding the MIN intensity for the RIGHT hand ---'
decision = 0
min_intens_right = MINintensity_right

while decision != '2':
   serialPort.write('i' + str(min_intens_right) + '\r\n')
   for motor_right in motors_right:
      serialPort.write('m' + str(motor_right) + '\r\n')
   decision = raw_input('Please enter the decision (1 for "too low", 2 for "ok" or 3 for "too high" or 4 to take a break): ')
   serialPort.write('a\r\n')
   if decision == '1' and min_intens_right != MAXintensity_right:
      min_intens_right = min_intens_right + step_right
   elif decision == '1' and min_intens_right == MAXintensity_right:
      print "It seems that the MAX intensity is too low to be perceived"
      break
   elif decision == '3' and min_intens_right == MINintensity_right:
      print "The experimenter should redefine the MIN intensity for the RIGHT hand as it is already too strong"
      break
   elif decision == '3' and min_intens_right != MINintensity_right:
      decision = raw_input('Are you sure? The previous one was too low and this one too high - Please enter the decision again (should be 1 for "too low" or 2 for "ok"): ')
   elif decision != '1' and decision != '2' and decision != '3':
      print 'Please enter a correct value between the ones proposed'

# STEP 1.4 - MAX intensity RIGHT

print '--- Finding the MAX intensity for the LEFT hand ---'
decision = '0'
max_intens_right = MAXintensity_right

while decision != '2':
   serialPort.write('i' + str(max_intens_right) + '\r\n')
   for motor_right in motors_right:
      serialPort.write('m' + str(motor_right) + '\r\n')
   decision = raw_input('Please enter the decision (1 for "too low", 2 for "ok" or 3 for "too high" or 4 to take a break): ')
   serialPort.write('a\r\n')
   if decision == '3' and max_intens_right != MINintensity_right:
      max_intens_right = max_intens_right - step_right
   elif decision == '3' and max_intens_right == MINintensity_right:
      print "It seems that the MIN intensity is too high and painful - to be adjusted"
      break
   elif decision == '1' and max_intens_right == MAXintensity_right:
      print "The experimenter should redefine the MAX intensity for the RIGHT hand as it is not perceivable (or the patient has no sensations?)"
      break
   elif decision == '1' and max_intens_right != MAXintensity_right:
      decision = raw_input('Are you sure? The previous one was too high and this one too low - Please enter the decision again (should be 2 for "ok" or 3 for "too high"): ')
   elif decision != '1' and decision != '2' and decision != '3':
      print 'Please enter a correct value between the ones proposed'

# STEP 1.5 - Writing the new parameters into the configuration file

file_obj = open(file_loc_name,'w')
file_obj.write("positive_only (Y for yes or N for no): Y" + "\n" + "-" + "\n")
file_obj.write("positive_bias (Y for yes or N for no): N" + "\n")
file_obj.write("strength-bias (in [0.0;0.5], with 1 decimal): 0.0" + "\n" + "-" + "\n")
file_obj.write("MIN_intensity_tactile-fb_left: " + str(min_intens_left) + "\n")
file_obj.write("MAX_intensity_tactile-fb_left: " + str(max_intens_left) + "\n")
file_obj.write("MIN_intensity_tactile-fb_right: " + str(min_intens_right) + "\n")
file_obj.write("MAX_intensity_tactile-fb_right: " + str(max_intens_right) + "\n" + "-" + "\n")
file_obj.close()


# ------------------------------------------------------------------------------
# STEP 2 - DETERMINE THE MIN PERCEIVABLE DIFFERENCE OF INTENSITY = DELTA
# ------------------------------------------------------------------------------
   
list_of_thresholds_left = []
list_of_thresholds_right = []

# nb of correct responses to the comparisons
performance_left = 0
performance_right = 0

### DELTA LEFT

print "COMPUTING THE NB OF THRESHOLDS FOR THE LEFT HAND"

tocontinue = '1'

# while the performance is not perfect (can be modified to make it easier)
# - nb comparisons = nb thresholds+1
# as we compare the neighbours 2 by 2
#while performance_left < nb_thresholds_left+1 :
while tocontinue == '1' or tocontinue == '2' :
   performance_left = 0
   list_of_thresholds_left = []
   # create the list containing the vibration intensities
   for i in range(nb_thresholds_left+1):
      list_of_thresholds_left.append((min_intens_left) + i*(int((max_intens_left-min_intens_left)/float(nb_thresholds_left))))
   # create the 2D matrix of the elements to be compared to check discriminability
   list_couples4comparison = []
   for i in range(len(list_of_thresholds_left)-1):
       list_couples4comparison.append([list_of_thresholds_left[i], list_of_thresholds_left[i+1]])
   # randomly shuffle the elements of this list for the comparison 
   shuffle(list_couples4comparison)
   for i in range(len(list_couples4comparison)):
      shuffle(list_couples4comparison[i])
   # launch the comparison (vibration of the 2 elements one after another)
   for i in range(len(list_couples4comparison)):
      for j in range(len(list_couples4comparison[i])):
         key = raw_input('- press to start vibration # ' + str(j+1))
         print '.... i = ', list_couples4comparison[i][j]
         serialPort.write('i' + str(list_couples4comparison[i][j]) + '\r\n')
         for motor_left in motors_left:
            serialPort.write('m' + str(motor_left) + '\r\n')
         stop = raw_input('- press any key to stop the vibration')
         serialPort.write('a\r\n')
   # getting the response of the participant
      raw_response = raw_input("\n **** Which vibration was the STRONGEST? (1 or 2) - ")
      try:
         response = int(raw_response)
      except ValueError:
         'Please enter a correct value between the ones proposed'
      while response != 1 and response != 2 :
          response = raw_input("**** Which vibration was the STRONGEST? (1 or 2) - ")
   # computing & displaying the performance
      if list_couples4comparison[i][response-1] == max(list_couples4comparison[i]):
          performance_left += 1
   print "\n **** Performance: ", performance_left, "/ ", nb_thresholds_left
   # choice : continue or stop
   tocontinue = raw_input("\n **** What do you want to do? 1 = try again / 2 = make it easier (reduce nb_thresholds) / 3 = stop, keep these parameters \n")
   if tocontinue == '2':
        nb_thresholds_left -= 1
   elif tocontinue == '3':
        file_obj = open(file_loc_name,'a')
        file_obj.write("nb_thresholds_tactile-fb_left: " + str(nb_thresholds_left) + "\n")
        file_obj.close()
   elif tocontinue != '1' and tocontinue != '2' and tocontinue != '3':
        tocontinue = raw_input("**** Please enter an appropriate value -- What do you want to do? 1 = try again / 2 = make it easier (reduce nb_thresholds) / 3 = stop, keep these parameters")



    
### DELTA RIGHT

print "COMPUTING THE NB OF THRESHOLDS FOR THE RIGHT HAND"

tocontinue = '1'

# while the performance is not perfect (can be modified to make it easier)
# - nb comparisons = nb thresholds+1
# as we compare the neighbours 2 by 2
#while performance_left < nb_thresholds_left+1 :
while tocontinue == '1' or tocontinue == '2' :
   performance_right = 0
   list_of_thresholds_right = []
   # create the list containing the vibration intensities
   for i in range(nb_thresholds_right+1):
      list_of_thresholds_right.append((min_intens_right) + i*(int((max_intens_left-min_intens_right)/float(nb_thresholds_right))))
   # create the 2D matrix of the elements to be compared to check discriminability
   list_couples4comparison = []
   for i in range(len(list_of_thresholds_right)-1):
       list_couples4comparison.append([list_of_thresholds_right[i], list_of_thresholds_right[i+1]])
   # randomly shuffle the elements of this list for the comparison 
   shuffle(list_couples4comparison)
   for i in range(len(list_couples4comparison)):
      shuffle(list_couples4comparison[i])
   # launch the comparison (vibration of the 2 elements the one after the one)
   for i in range(len(list_couples4comparison)):
      for j in range(len(list_couples4comparison[i])):
         key = raw_input('- press to start vibration # ' + str(j+1))
         print '.... i = ', str(list_couples4comparison[i][j])
         serialPort.write('i' + str(list_couples4comparison[i][j]) + '\r\n')
         for motor_right in motors_right:
            serialPort.write('m' + str(motor_right) + '\r\n')
         stop = raw_input('- press any key to stop the vibration')
         serialPort.write('a\r\n')
   # getting the response of the participant
      raw_response = raw_input("\n **** Which vibration was the STRONGEST? (1 or 2) - ")
      try:
         response = int(raw_response)
      except ValueError:
         'Please enter a correct value between the ones proposed'
      while response != 1 and response != 2 :
          response = raw_input("**** Which vibration was the STRONGEST? (1 or 2) - ")
   # computing & displaying the performance
      if list_couples4comparison[i][response-1] == max(list_couples4comparison[i]):
          performance_right += 1
   print "\n **** Performance: ", performance_right, "/ ", nb_thresholds_right
   # choice : continue or stop
   tocontinue = raw_input("\n **** What do you want to do? 1 = try again / 2 = make it easier (reduce nb_thresholds) / 3 = stop, keep these parameters \n")
   if tocontinue == '2':
        nb_thresholds_right -= 1
   elif tocontinue == '3':
        file_obj = open(file_loc_name,'a')
        file_obj.write("nb_thresholds_tactile-fb_right: " + str(nb_thresholds_right) + "\n")
        file_obj.close()
   elif tocontinue != '1' and tocontinue != '2' and tocontinue != '3':
        tocontinue = input("**** Please enter an appropriate value -- What do you want to do? 1 = try again / 2 = make it easier (reduce nb_thresholds) / 3 = stop, keep these parameters")
    
   
      

      
   
   

    
    
    
