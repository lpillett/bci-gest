#!/usr/bin/python

# ------------------------------------------------------------------------------
# Author : lea pillette
# Date : 11-04-2017
# Purpose : XP stroke - tactile & visual feedback
# Run Arduino program motor.ino -- tactile feedback (e.g., positive only, biased)
# ------------------------------------------------------------------------------

# add pylsl to known path
from math import copysign
import sys; sys.path.append('./pylsl') 
from pylsl import StreamInlet, resolve_stream

import serial
import time
import random
import math
   
# Windows
serialPort = serial.Serial('COM14', 9600, timeout=1)

# enter the participant ID and the run number
S = input('Please enter the ID of the participant: ')
#r = input('Please enter the run number: ')

# ------------------------------------------------------------------------------
# GET THE PARAMETERS FROM THE CONFIG FILE
# ------------------------------------------------------------------------------

# opens the configuration file
file = open(('openvibe_xp_files_and_signals/signals/configS' + str(S) + '.txt'), 'r')
lines = file.readlines()

# positive only: Y (for yes) or N (for no) - if Y, no negative feedback
positive_only = lines[0][-2]

# positive_bias: if Y, the feedback is switched of "strength_bias" in the correct direction 
positive_bias = lines[2][-2]
strength_bias = lines[3][-4:-1]

# to get the min and max intensities on the left hand
# and the smallest perceivable difference of intensities on the left hand
MINintensity_left = float(lines[5].split(': ')[1][0:len(lines[5].split(': ')[1])-1])
MAXintensity_left = float(lines[6].split(': ')[1][0:len(lines[6].split(': ')[1])-1])
nb_thresholds_left = int(float(lines[10].split(': ')[1][0:len(lines[10].split(': ')[1])-1]))
#print (MAD_left)

# to get the min and max intensities on the left hand
# and the smallest perceivable difference of intensities on the left hand
MINintensity_right = float(lines[7].split(': ')[1][0:len(lines[7].split(': ')[1])-1])
MAXintensity_right = float(lines[8].split(': ')[1][0:len(lines[8].split(': ')[1])-1])
nb_thresholds_right = int(float(lines[11].split(': ')[1][0:len(lines[11].split(': ')[1])-1]))
#print (MAD_right)

file.close()


# ------------------------------------------------------------------------------
# SET THE PARAMETERS FOR THE SESSION
# ------------------------------------------------------------------------------

#file = open(('openvibe_xp_files_and_signals/signals/configS' + str(S) + 'r' + str(r) + '.txt'),'a')

# set the normal number of thresholds between the min and max intensities
threshold_left = int((MAXintensity_left - MINintensity_left) / nb_thresholds_left)
threshold_right = int((MAXintensity_right - MINintensity_right) / nb_thresholds_right)

# create a list containing the values of the classifier output for which the
# intensity will change on the left hand
##change_intensity_left = 0.5 / nb_thresholds_left
##List_thresholds_left = [0]*(nb_thresholds_left + 1)
##for i in range (len(List_thresholds_left)):
##   List_thresholds_left[i] = i * change_intensity_left

# create a list containing the values of the classifier output for which the
# intensity will change on the right hand	
##change_intensity_right = 0.5 / nb_thresholds_right
##List_thresholds_right = [0]*(nb_thresholds_right +1)
##for i in range (len(List_thresholds_right)):
##   List_thresholds_right[i] = i * change_intensity_right
# write the threshold value & number for each hand in the config file
#file.write('\r\n threshold_left_tact: ' + str(threshold_left))
#file.write('\r\n nb_thresholds_left_tact: ' + str(nb_thresholds_left))
#file.write('\r\n threshold_right_tact: ' + str(threshold_right))
#file.write('\r\n nb_thresholds_right_tact: ' + str(nb_thresholds_right))

#file.close()


# ------------------------------------------------------------------------------
# CONNECTION TO ARDUINO
# ------------------------------------------------------------------------------

# wait for Arduino setup to finish
time.sleep(2)
connected = False
print 'Waiting for Arduino...'
while not connected:
   serin = serialPort.readline()
   if serin == 'running motor.ino\r\n':
      print 'A: ' + serin
      connected = True
   else:
      print 'A: ' + serin

# wait for motors to wake up (a small mistake in the motor shield :-)
time.sleep(2);
   

# ------------------------------------------------------------------------------
# COMMUNICATION WITH OPENVIBE
# ------------------------------------------------------------------------------

# first resolve an EEG stream on the lab network
print("Looking for the stream...")
streams = resolve_stream('type', 'XPstroke_FBtact')
# create a new inlet to read from the stream
inlet = StreamInlet(streams[0])
print("Stream found")


# ------------------------------------------------------------------------------
# ACTION
# ------------------------------------------------------------------------------

stim = 0
intensity = 0
instruction = 0
motors_left = [0, 1] # id of the motor to be activated (can be modified)
motors_right = [4, 5] # id of the motor to be activated (can be modified)

while True:

   stim = 0   
   # get a new sample (you can also omit the timestamp part if you're not
   # interested in it)
   sample, timestamp = inlet.pull_sample()
   if sample[1] != 0:
      stim = sample[1]
      print(str(stim))

   # to display the current intruction
   if stim == 769 :
      instruction = 769
   elif stim == 770 :
      instruction = 770
   elif stim == 800 :
      serialPort.write('a\r\n')
   
   # to stop the motors at the end of a trial
   elif stim == 32770:
      instruction = 32770
      serialPort.write('a\r\n')
   
   # to send the command to the Arduino
   elif stim >= 33024 and stim <= 33030 :
      serialPort.write('a\r\n')
      index = int(stim - 33024)
      print(str(index))
      if index == 0 :
         serialPort.write('a\r\n')
      elif instruction == 769 : #left
         intensity_left = MINintensity_left + threshold_left * index
         serialPort.write('i' + str(intensity_left) + '\r\n')
         for motor_left in motors_left:
            serialPort.write('m' + str(motor_left) + '\r\n')
      elif instruction == 770 and sample[0] >= 0 : #right
         intensity_right = MINintensity_right + threshold_right * index
         serialPort.write('i' + str(intensity_right) + '\r\n')
         for motor_right in motors_right:
            serialPort.write('m' + str(motor_right) + '\r\n')


serialPort.write('a\r\n')    
