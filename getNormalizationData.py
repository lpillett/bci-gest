#!/usr/bin/python

# ------------------------------------------------------------------------------
# Authors : léa pillette
# Date : 16-03-2019
# Purpose : XP stroke - tactile & visual feedback
# ------------------------------------------------------------------------------

#dataframe processing
import pandas as pd

#display only 10 rows of the dataframe
pd.options.display.max_rows = 10

#setting variables
instruction = ' '
fbDisplayed = False
lineNumber = -1
trialStart = 0
varMedianRight = pd.DataFrame()
nb_right = 0
varMedianLeft = pd.DataFrame()
nb_left = 0

#storing the LDA outputs
LDArightOutputs = pd.DataFrame()
LDAleftOutputs = pd.DataFrame()

#enter the participant ID
sID = input("Please enter the ID of the participant: ")
session = input("Please enter the number of the session: ")
run_first = input("Please enter the number of the first run: ")
run_last = input("Please enter the number of the last run: ")

#dataframe creation ", sID, "
csvFileRight = ("openvibe_xp_files_and_signals/signals/XPfeedback_S" + sID + "_Session" + session + "_r" + run_first + "to" + run_last + "_classifier_output_tonorm_right.csv")
cOR = pd.read_csv(csvFileRight,sep=',')
cOR = cOR.rename(index=str, columns={'Unnamed: 2':'LDA_1','Unnamed: 3':'LDA_2'})
classifOutputRight = cOR.set_index("Event Id", drop = False)
classifOutputRight['Feedback']=pd.Series(0, index=classifOutputRight.index)

csvFileLeft = ("openvibe_xp_files_and_signals/signals/XPfeedback_S" + sID + "_Session" + session + "_r" + run_first + "to" + run_last + "_classifier_output_tonorm_left.csv")
cOL = pd.read_csv(csvFileLeft,sep=',')
cOL = cOL.rename(index=str, columns={'Unnamed: 2':'LDA_1','Unnamed: 3':'LDA_2'})
classifOutputLeft = cOL.set_index("Event Id", drop = False)
classifOutputLeft["Feedback"]=pd.Series(0, index=classifOutputLeft.index)


#calculate the MAD
for stimulation in classifOutputLeft["Event Id"]:
    lineNumber += 1
    #print(lineNumber)
    
    #trial right
    if stimulation == '770':
        instruction = 'R'
        nb_right += 1
        
    #trial left
    elif stimulation == '769':
        instruction = 'L'
        nb_left += 1
        
    #baseline start
    elif stimulation == '780':
        trialStart = lineNumber
        instruction = 'U'
        fbDisplayed = True
    
    #stimulation target 
    elif stimulation == '33285':
        trialStart = lineNumber
        fbDisplayed = True
        
    #trial end or baseline stop
    elif (stimulation == '32776' or stimulation == '33286'):
        fbDisplayed = False
        if instruction == 'R':
            LDArightOutputs = pd.concat([LDArightOutputs,classifOutputRight[trialStart:lineNumber-1]["Feedback"]]) #, ignore_index=True
        elif instruction == 'L':
            LDAleftOutputs = pd.concat([LDAleftOutputs,classifOutputLeft[trialStart:lineNumber-1]["Feedback"]])
        elif instruction == 'U':
            LDArightOutputs = pd.concat([LDArightOutputs,classifOutputRight[trialStart:lineNumber-1]["Feedback"]])
            LDAleftOutputs = pd.concat([LDAleftOutputs,classifOutputLeft[trialStart:lineNumber-1]["Feedback"]])
            
    #If their is a feedback made compute the performance
    if fbDisplayed :
        if instruction == 'R':
            xRight=(abs(classifOutputRight[lineNumber:lineNumber+1]["LDA_2"])-abs(classifOutputRight[lineNumber:lineNumber+1]["LDA_1"]))/(abs(classifOutputRight[lineNumber:lineNumber+1]["LDA_1"])+abs(classifOutputRight[lineNumber:lineNumber+1]["LDA_2"]))
            classifOutputRight.iloc[lineNumber:lineNumber+1,7:8]=xRight
        elif instruction == 'L':
            xLeft=(abs(classifOutputLeft[lineNumber:lineNumber+1]["LDA_2"])-abs(classifOutputLeft[lineNumber:lineNumber+1]["LDA_1"]))/(abs(classifOutputLeft[lineNumber:lineNumber+1]["LDA_1"])+abs(classifOutputLeft[lineNumber:lineNumber+1]["LDA_2"]))
            classifOutputLeft.iloc[lineNumber:lineNumber+1,7:8]=xLeft 
        elif instruction == 'U':
            xRight=(abs(classifOutputRight[lineNumber:lineNumber+1]["LDA_2"])-abs(classifOutputRight[lineNumber:lineNumber+1]["LDA_1"]))/(abs(classifOutputRight[lineNumber:lineNumber+1]["LDA_1"])+abs(classifOutputRight[lineNumber:lineNumber+1]["LDA_2"]))
            xLeft=(abs(classifOutputLeft[lineNumber:lineNumber+1]["LDA_2"])-abs(classifOutputLeft[lineNumber:lineNumber+1]["LDA_1"]))/(abs(classifOutputLeft[lineNumber:lineNumber+1]["LDA_1"])+abs(classifOutputLeft[lineNumber:lineNumber+1]["LDA_2"]))
            classifOutputRight.iloc[lineNumber:lineNumber+1,7:8]=xRight
            classifOutputLeft.iloc[lineNumber:lineNumber+1,7:8]=xLeft

print("Nb trial right: " + str(nb_right) + "   -   Nb trial left : " + str(nb_left))

#compute the MAD
#print(LDArightOutputs)
medianRight = LDArightOutputs.median()
medianLeft = LDAleftOutputs.median()

for perfRight in range(0, len(LDArightOutputs)):
    #print("Perf Right" + str(LDArightOutputs[0][perfRight]))
    varRight = abs(LDArightOutputs[0][perfRight] - medianRight)
    varMedianRight = pd.concat([varMedianRight, varRight])
for perfLeft in range(0, len(LDAleftOutputs)):
    #print("Perf Left" + str(LDArightOutputs[0][perfLeft]))
    varLeft = abs(LDAleftOutputs[0][perfLeft] - medianLeft)
    varMedianLeft = pd.concat([varMedianLeft, varLeft])

MADright = varMedianRight.median()
MADleft = varMedianLeft.median()
print("MAD computed")

#write the MAD of each classifiers in the config file
file = open(('openvibe_xp_files_and_signals/signals/configS' + sID + '.txt'),'a')
file.write('\nMedian_left: ' + str(medianLeft.get_values()[0]))
file.write('\nMAD_left: ' + str(MADleft.get_values()[0]))
file.write('\nMedian_right: ' + str(medianRight.get_values()[0]))
file.write('\nMAD_right: ' + str(MADright.get_values()[0]))
file.close()

print("Config saved")
