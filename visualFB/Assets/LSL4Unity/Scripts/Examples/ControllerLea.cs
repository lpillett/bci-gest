﻿using UnityEngine;
using System;
using System.Linq;
using Assets.LSL4Unity.Scripts.AbstractInlets;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Assets.LSL4Unity.Scripts.Examples
{

    public class ControllerLea : AFloatInlet
    {
        #region Define variables
        //Public 
        public int subjectID;
        public GameObject human;
        public GameObject blackScreen;
        public GameObject cross;
        public GameObject leftArrow;
        public GameObject rightArrow;
        public GameObject txtRest;
        public float current_speed;

        //Private
        Animator animHuman;
        string lastSample = String.Empty;
        private Dictionary<string, string> dictStim;
        private bool ongoingFB;
        private bool is_rest;
        private bool is_perf_to_save;
        private string last_stimulation;
        private char trial_type;

        private bool positive_fb;
        private bool biased_fb;
        private float bias;
        private int closest_index;
        private float mean_perf_trial_rest_left;
        private float mean_perf_trial_rest_right;

        private float min_intensity_left;
        private float max_intensity_left;
        private float nb_thresholds_left;
        private float median_left;
        private float MAD_left;
        private float threshold_left;
        private List<float> list_of_thresholds_left;
        private float perf_left;
        private float nb_window_recognized_left;
        private int nb_trial_left;
        private float mean_perf_trial_left;

        private float min_intensity_right;
        private float max_intensity_right;
        private float nb_thresholds_right;
        private float median_right;
        private float MAD_right;
        private float threshold_right;
        private List<float> list_of_thresholds_right;
        private float perf_right;
        private float nb_window_recognized_right;
        private int nb_trial_right;
        private float mean_perf_trial_right;

        private int nb_window_rest;
        private int nb_window_left;
        private int nb_window_right;
        #endregion

        /// <summary>
        /// Is called once when the program starts
        /// </summary>
        private void Awake() {
            //Initialize variables
            dictStim = CreateDictStim();
            animHuman = human.GetComponent<Animator>();
            ongoingFB = false;
            is_rest = false;
            is_perf_to_save = false;
            trial_type = ' ';
            nb_window_rest = 0;
            nb_window_left = 0;
            nb_window_right = 0;

            nb_window_recognized_left = 0;
            mean_perf_trial_left = 0;
            nb_trial_left = 0;
            nb_window_recognized_right = 0;
            mean_perf_trial_right = 0;
            nb_trial_right = 0;

            //Disable the 2D elements
            cross.SetActive(false);
            rightArrow.SetActive(false);
            leftArrow.SetActive(false);
            txtRest.SetActive(false);

            //Get config information regarding the participant from the OpenViBE config file
            //TO DO: change the path for the one where the OpenViBE config file containing the ID of your participant is
            string[] linesOVconf = System.IO.File.ReadAllLines(@"C:\Your_path_to_the_OpenViBE_folder\openvibe-2.2.0\share\openvibe\kernel\openvibe.conf");
            subjectID = int.Parse(linesOVconf[16].Split('=')[1]);

            //TO DO: change the path for the one where the config file containing the parameters for your participant is
            string[] lines = System.IO.File.ReadAllLines(@"C:\Your_path_to_the_config_file\configS" + subjectID.ToString() + ".txt");

            positive_fb = (lines[0][lines[0].Length - 1] == 'Y') ? true : false;
            biased_fb = (lines[2][lines[0].Length - 1] == 'Y') ? true : false;
            bias = float.Parse(lines[3].Split(':')[1]);
            
            min_intensity_left = float.Parse(lines[13].Split(':')[1]);
            max_intensity_left = float.Parse(lines[14].Split(':')[1]);
            nb_thresholds_left = float.Parse(lines[18].Split(':')[1]);
            median_left = float.Parse(lines[21].Split(':')[1]);
            MAD_left = float.Parse(lines[22].Split(':')[1]);

            min_intensity_right = float.Parse(lines[15].Split(':')[1]);
            max_intensity_right = float.Parse(lines[16].Split(':')[1]);
            nb_thresholds_right = float.Parse(lines[19].Split(':')[1]);
            median_right = float.Parse(lines[23].Split(':')[1]);
            MAD_right = float.Parse(lines[24].Split(':')[1]);

            //Compute the thresholds for visual FB speed
            threshold_left = (float)((max_intensity_left - min_intensity_left) / nb_thresholds_left);
            threshold_right = (float)((max_intensity_right - min_intensity_right) / nb_thresholds_right);
            list_of_thresholds_left = new List<float>();
            for (int i = 0; i < nb_thresholds_left + 1; i++) {
                list_of_thresholds_left.Add(min_intensity_left + i * threshold_left);
            }
            list_of_thresholds_right = new List<float>();
            for (int i = 0; i < nb_thresholds_right + 1; i++) {
                list_of_thresholds_right.Add(min_intensity_right + i * threshold_right);
            }
        }

        /// <summary>
        /// Is called once per frame
        /// </summary>
        private void Update() {
            //Used to have the scripts running even when the focus is not on the Unity window
            Application.runInBackground = true;
        }

        /// <summary>
        /// Is called everytime there is a new LSL stream received
        /// </summary>
        /// <param name="newSample"></param>
        /// <param name="timeStamp"></param>
        protected override void Process(float[] newSample, double timeStamp)
        {
            //Segment the LSLstream
            lastSample = string.Join(" ", newSample.Select(c => c.ToString()).ToArray());
            
            //String containing the last stimulation
            last_stimulation = dictStim[newSample[4].ToString()];

            //If a stimulation was received
            if (last_stimulation != "NoValue")
            {
                //Display the stimulation in the logs
                Debug.Log(
                        string.Format("Stimulation : {0}", last_stimulation)
                );
                if (last_stimulation == "OVTK_GDF_Start_Of_Trial") {
                    //Display cross
                    cross.SetActive(true);
                } else if (last_stimulation == "OVTK_GDF_Up") {
                    //Display text Rest
                    txtRest.SetActive(true);
                    is_rest = true;
                } else if (last_stimulation == "OVTK_StimulationId_BaselineStop") {
                    //Remove text Rest
                    txtRest.SetActive(false);
                    is_rest = false;
                } else if (last_stimulation == "OVTK_GDF_Left") {
                    //Display left arrow and save trial type
                    nb_trial_left++;
                    leftArrow.SetActive(true);
                    trial_type = 'L';
                } else if (last_stimulation == "OVTK_GDF_Right") {
                    //Display right arrow and save trial type
                    nb_trial_right++;
                    rightArrow.SetActive(true);
                    trial_type = 'R';
                } else if (last_stimulation == "OVTK_GDF_Feedback_Continuous") {
                    //Remove arrows and start FB
                    leftArrow.SetActive(false);
                    rightArrow.SetActive(false);
                    ongoingFB = true;
                } else if (last_stimulation == "OVTK_StimulationId_Target") {
                    is_perf_to_save = true;
                } else if (last_stimulation == "OVTK_StimulationId_NonTarget") {
                    is_perf_to_save = false;
                } else if (last_stimulation == "OVTK_GDF_End_Of_Trial") {
                    //Initialize the variables for the next trial
                    ongoingFB = false;
                    trial_type = ' ';
                    cross.SetActive(false);
                    rightArrow.SetActive(false);
                    leftArrow.SetActive(false);
                    txtRest.SetActive(false);
                    animHuman.SetTrigger("ToRest");
                    animHuman.speed = 1;
                } else if (last_stimulation == "OVTK_StimulationId_ExperimentStop") {
                    //Remove black screen
                    blackScreen.SetActive(true);
                }
            }

            //If their is a feedback displayed
            if (is_rest || ongoingFB) {
                //Compute the current performance for each classifier
                perf_left = (((Math.Abs(newSample[1]) - Math.Abs(newSample[0])) / (Math.Abs(newSample[1]) + Math.Abs(newSample[0]))) - median_left) / (MAD_left*2);
                perf_right = (((Math.Abs(newSample[3]) - Math.Abs(newSample[2])) / (Math.Abs(newSample[3]) + Math.Abs(newSample[2]))) - median_right) / (MAD_right*2);

                //Display the performances
                Debug.Log(
                        string.Format("PerfLeft : {0}    PerfRight : {1}", perf_left, perf_right)
                );

                //Save if the task is recognized for this window and adapt the speed accordinginly
                if (ongoingFB) {
                    if (trial_type == 'L' && is_perf_to_save) { 
                        nb_window_left++;
                        if (perf_left >= 0)
                            nb_window_recognized_left++;
                    }
                    else if (trial_type == 'R' && is_perf_to_save) {
                        nb_window_right++;
                        if (perf_right >= 0)
                            nb_window_recognized_right++;
                    }
                    if (trial_type == 'L' & perf_left >= 0) {
                        animHuman.SetTrigger("ToMoveLeft");
                        closest_index = ComputeClosestIndex(list_of_thresholds_left, perf_left);
                        animHuman.speed = min_intensity_left + threshold_left * closest_index;
                    } 
                    else if (trial_type == 'R' & perf_right >= 0) {
                        animHuman.SetTrigger("ToMoveRight");
                        closest_index = ComputeClosestIndex(list_of_thresholds_right, perf_right);
                        animHuman.speed = min_intensity_right + threshold_right * closest_index;
                    } else {
                        animHuman.speed = 0;
                    }
                }
                //If rest perf
                if (is_rest) {
                    nb_window_rest++;
                    if (perf_left <= 0)
                        nb_window_recognized_left++;
                    if (perf_right <= 0)
                        nb_window_recognized_right++;
                }
            }
            else {
                animHuman.SetTrigger("ToRest");
            }
            //Update the variable on Unity window
            current_speed = animHuman.speed;
        }

        /// <summary>
        /// Find the closest threshold to the last performance to adapt the speed accordingly
        /// </summary>
        /// <param name="thresholds"></param>
        /// <param name="performance"></param>
        /// <returns></returns>
        private int ComputeClosestIndex(List<float>thresholds, float performance) {
            List < float> diff = new List<float>();
            for (int i=0; i<thresholds.Count; i++) {
                diff.Add(Math.Abs(thresholds[i] - performance));
            }
            Debug.Log(
                string.Format("Performance:{0} Threshold:{1}", performance, diff.IndexOf(diff.Min()))
                );
            return diff.IndexOf(diff.Min());
        }

        /// <summary>
        /// Is called once when the application is closing
        /// </summary>
        void OnApplicationQuit() {
            //Save the mean performances over the run
            if (nb_trial_left!=0 & nb_trial_right!= 0) {
                //TO DO: change the path for the one where the performances of the participant are saved
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Your_path_to_file_where_perfs_are_saved\online_performances_S" + subjectID.ToString() + ".txt", true)) {
					file.WriteLine("Run -  - Mean performance right: " + (nb_window_recognized_right * 100 / (nb_window_rest + nb_window_right)).ToString() + " - Mean performance left: " + (nb_window_recognized_left * 100 / (nb_window_rest + nb_window_left)).ToString() + " (NB data rest: " + nb_window_rest.ToString()  + " NB data left:" + nb_window_left.ToString() + " NB data right: " + nb_window_right.ToString() + ")");
				}
			}
        }

        /// <summary>
        /// Contains the equivalent names of OpenViBE stimulation IDs
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> CreateDictStim() {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["32769"] = "OVTK_StimulationId_ExperimentStart";
            dict["32770"] = "OVTK_StimulationId_ExperimentStop";
            dict["33282"] = "OVTK_StimulationId_Beep";
            dict["33281"] = "OVTK_StimulationId_Train";
            dict["1010"] = "OVTK_GDF_End_Of_Session";
            dict["768"] = "OVTK_GDF_Start_Of_Trial";
            dict["786"] = "OVTK_GDF_Cross_On_Screen";
            dict["32775"] = "OVTK_StimulationId_BaselineStart";
            dict["32776"] = "OVTK_StimulationId_BaselineStop";
            dict["769"] = "OVTK_GDF_Left";
            dict["770"] = "OVTK_GDF_Right";
            dict["780"] = "OVTK_GDF_Up";
            dict["33285"] = "OVTK_StimulationId_Target";
            dict["33286"] = "OVTK_StimulationId_NonTarget";
            dict["781"] = "OVTK_GDF_Feedback_Continuous";
            dict["800"] = "OVTK_GDF_End_Of_Trial";
            dict["0"] = "NoValue";
            return dict;
        }
    }
}