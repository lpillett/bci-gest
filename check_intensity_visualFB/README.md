Author : léa pillette
Date : 16-03-2019
Purpose : Brain-Computer Interface Grasping Exercise for uSer Training (BCI-GEST) 

This Unity program (to run without OpenViBE) enables to find the adapted parameters of the visual feedback for a participant.