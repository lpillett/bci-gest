function initialize(box)

	dofile(box:get_config("${Path_Data}") .. "/plugins/stimulation/lua-stimulator-stim-codes.lua")

	nbr_trials_hand_speed = box:get_setting(2)
	first_class = _G[box:get_setting(3)]
	second_class = _G[box:get_setting(4)]
	baseline_duration = box:get_setting(5)
	nbr_speed_per_trial = box:get_setting(6)
	feedback_duration = box:get_setting(7)
	nbr_speed = 7
	nb_class = 2
	
	nbr_trials_per_class = nbr_speed * nbr_trials_hand_speed
	
	-- initializes random seed
	math.randomseed(os.time())

	-- fill the sequence table with predifined order
	sequence = {}
	for i = 1, nbr_trials_per_class do
		table.insert(sequence, 1, first_class)
		table.insert(sequence, 1, second_class)
	end

	-- randomize the sequence
	for i = 1, nbr_trials_per_class do
		a = math.random(1, nbr_trials_per_class*2)
		b = math.random(1, nbr_trials_per_class*2)
		swap = sequence[a]
		sequence[a] = sequence[b]
		sequence[b] = swap
	end
	
	-- fill the sequence table with predifined order
	sequence_left = {}
	sequence_right = {}
	for i = 1, nbr_trials_per_class * nbr_speed_per_trial do
		table.insert(sequence_left, 1, OVTK_StimulationId_Label_00)
		table.insert(sequence_left, 1, OVTK_StimulationId_Label_01)
		table.insert(sequence_left, 1, OVTK_StimulationId_Label_02)
		table.insert(sequence_left, 1, OVTK_StimulationId_Label_03)
		table.insert(sequence_left, 1, OVTK_StimulationId_Label_04)
		table.insert(sequence_left, 1, OVTK_StimulationId_Label_05)
		table.insert(sequence_left, 1, OVTK_StimulationId_Label_06)
		table.insert(sequence_right, 1, OVTK_StimulationId_Label_00)
		table.insert(sequence_right, 1, OVTK_StimulationId_Label_01)
		table.insert(sequence_right, 1, OVTK_StimulationId_Label_02)
		table.insert(sequence_right, 1, OVTK_StimulationId_Label_03)
		table.insert(sequence_right, 1, OVTK_StimulationId_Label_04)
		table.insert(sequence_right, 1, OVTK_StimulationId_Label_05)
		table.insert(sequence_right, 1, OVTK_StimulationId_Label_06)
	end
	
	-- randomize the sequence_left
	for i = 1, nbr_trials_per_class * nbr_speed_per_trial do
		a = math.random(1, nbr_trials_per_class * nbr_speed_per_trial)
		b = math.random(1, nbr_trials_per_class * nbr_speed_per_trial)
		swap = sequence_left[a]
		sequence_left[a] = sequence_left[b]
		sequence_left[b] = swap
	end
	
	-- randomize the sequence_right
	for i = 1, nbr_trials_per_class * nbr_speed_per_trial do
		a = math.random(1, nbr_trials_per_class * nbr_speed_per_trial)
		b = math.random(1, nbr_trials_per_class * nbr_speed_per_trial)
		swap = sequence_right[a]
		sequence_right[a] = sequence_right[b]
		sequence_right[b] = swap
	end

end

function process(box)
	local t=0
	
	-- manages baseline
	box:send_stimulation(1, OVTK_StimulationId_ExperimentStart, t, 0)
	t = t + baseline_duration
	
	-- manages trials
	for i = 1, nbr_trials_per_class*nb_class do
	
		-- display cue
		box:send_stimulation(1, sequence[i], t, 0)
		t = t + 1.25
		
		-- provide feedback
		if sequence[i] == first_class then
			for j = 1, nbr_speed_per_trial do
				box:send_stimulation(1, sequence_left[i * j], t, 0)
				t = t + (feedback_duration / nbr_speed_per_trial)
			end
		elseif sequence[i] == second_class then
			for j = 1, nbr_speed_per_trial do
				box:send_stimulation(1, sequence_right[i * j], t, 0)
				t = t + (feedback_duration / nbr_speed_per_trial)
			end
		end
		
		-- ends trial
		box:send_stimulation(1, OVTK_GDF_End_Of_Trial, t, 0)
		t = t + math.random(1.25, 2.25)
	end
	
	-- send end for completeness	
	box:send_stimulation(1, OVTK_GDF_End_Of_Session, t, 0)
	t = t + 5
	
	-- used to cause the acquisition scenario to stop
	box:send_stimulation(1, OVTK_StimulationId_ExperimentStop, t, 0)
end











