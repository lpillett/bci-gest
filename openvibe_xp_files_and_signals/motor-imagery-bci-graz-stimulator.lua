
function initialize(box)

	dofile(box:get_config("${Path_Data}") .. "/plugins/stimulation/lua-stimulator-stim-codes.lua")

	number_of_trials = box:get_setting(2) -- 10 trials per class
	first_class = _G[box:get_setting(3)] -- OVTK_GDF_Left
	second_class = _G[box:get_setting(4)] -- OVTK_GDF_Right
	baseline_duration = box:get_setting(5) -- 5 sec
	wait_for_beep_duration = box:get_setting(6) -- 1 sec
	wait_for_cue_duration = box:get_setting(7) -- 2.5 sec
	display_cue_duration = box:get_setting(8) -- 1.25 sec
	feedback_duration = box:get_setting(9) -- 5 sec
	end_of_trial_min_duration = box:get_setting(10) -- 3.5 sec
	end_of_trial_max_duration = box:get_setting(11) -- 4.5 sec

	-- initializes random seed
	math.randomseed(os.time())

	-- fill the sequence table with predifined order
	sequence = {}
	for i = 1, number_of_trials do
		table.insert(sequence, 1, first_class)
		table.insert(sequence, 1, second_class)
	end

	-- randomize the sequence
	for i = 1, number_of_trials do
		a = math.random(1, number_of_trials*2)
		b = math.random(1, number_of_trials*2)
		swap = sequence[a]
		sequence[a] = sequence[b]
		sequence[b] = swap
	end

end

function process(box)

	local t=0

	-- manages baseline
	box:send_stimulation(1, OVTK_StimulationId_ExperimentStart, t, 0)
	t = t + baseline_duration

	-- manages trials

	for i = 1, number_of_trials*2 do

		-- first display cross on screen

		box:send_stimulation(1, OVTK_GDF_Start_Of_Trial, t, 0)
		box:send_stimulation(1, OVTK_GDF_Cross_On_Screen, t, 0)
		t = t + wait_for_beep_duration

		-- warn the user the cue is going to appear

		box:send_stimulation(1, OVTK_StimulationId_Beep, t, 0)
		t = t + 0.5
		box:send_stimulation(1, OVTK_GDF_Up, t, 0)
		t = t + wait_for_cue_duration
		box:send_stimulation(1, OVTK_StimulationId_BaselineStop, t, 0)
		t = t + 0.5

		-- display cue

		box:send_stimulation(1, sequence[i], t, 0)
		t = t + display_cue_duration

		-- provide feedback

		box:send_stimulation(1, OVTK_GDF_Feedback_Continuous, t, 0)
		t = t + 0.5
		box:send_stimulation(1, OVTK_StimulationId_Target, t, 0)
		t = t + feedback_duration
		box:send_stimulation(1, OVTK_StimulationId_NonTarget, t, 0)
		t = t + 0.5

		-- ends trial

		box:send_stimulation(1, OVTK_GDF_End_Of_Trial, t, 0)
		t = t + math.random(end_of_trial_min_duration, end_of_trial_max_duration)

	end

	-- send end for completeness	
	box:send_stimulation(1, OVTK_GDF_End_Of_Session, t, 0)
	t = t + 5

	box:send_stimulation(1, OVTK_StimulationId_Train, t, 0)
	t = t + 1
	
	-- used to cause the acquisition scenario to stop
	box:send_stimulation(1, OVTK_StimulationId_ExperimentStop, t, 0)
	
end
