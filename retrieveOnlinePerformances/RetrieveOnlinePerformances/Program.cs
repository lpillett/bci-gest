﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace RetrieveOnlinePerformances {
    class Program {
        static void Main(string[] args) {

            #region Dictionnary of stims
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["32769"] = "OVTK_StimulationId_ExperimentStart";
            dict["32770"] = "OVTK_StimulationId_ExperimentStop";
            dict["32775"] = "OVTK_StimulationId_BaselineStart";
            dict["32776"] = "OVTK_StimulationId_BaselineStop";
            dict["33282"] = "OVTK_StimulationId_Beep";
            dict["33281"] = "OVTK_StimulationId_Train";
            dict["768"] = "OVTK_GDF_Start_Of_Trial";
            dict["769"] = "OVTK_GDF_Left";
            dict["770"] = "OVTK_GDF_Right";
            dict["780"] = "OVTK_GDF_Up";
            dict["33285"] = "OVTK_StimulationId_Target";
            dict["33286"] = "OVTK_StimulationId_NonTarget";
            dict["781"] = "OVTK_GDF_Feedback_Continuous";
            dict["786"] = "OVTK_GDF_Cross_On_Screen";
            dict["800"] = "OVTK_GDF_End_Of_Trial";
            dict["1010"] = "OVTK_GDF_End_Of_Session";
            dict["150"] = "calib";
            dict[""] = "NoValue";
            dict["768:786"] = "do not care";
            dict["1010"] = "do not care";
            dict["33281"] = "do not care";
            #endregion

            #region Init variables
            string last_stimulation = "";
            string[] line_left;
            string[] line_right;

            char trialType;
            bool is_rest;
            bool is_perf_to_save;

            float nb_window_recognized_left;
            float nb_window_recognized_right;
            float nb_fb_window;
            float nb_window_rest;
            float nb_window_left;
            float nb_window_right;


            int nb_trial_left;
            int nb_trial_right;

            float perf_left;
            float perf_right;
            float run_performance;

            float LDA1_left;
            float LDA2_left;
            float LDA1_right;
            float LDA2_right;

            string[] lines_file_left;
            string[] lines_file_right;

            int nbR2save = 0;
            #endregion

            //Ask information to then open the correct EEG files
            Console.WriteLine("Enter the ID of the subject :");
            string subjectID = Console.ReadLine();
            
            Console.WriteLine("Enter the number of the session :");
            string session = Console.ReadLine();

            Console.WriteLine("Enter the number of the first run to compute :");
            int nbR1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter the number of the last run to compute :");
            int nbR2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Do you want to compute the performances of several files (if true enter 1, if not enter 2)");
            bool is_concat = (int.Parse(Console.ReadLine()) == 2 ? true : false);

            //If there are several Runs inside a concat file to process 
            if (is_concat) {
                nbR2save = nbR2;
                nbR2 = nbR1;
            }

            //Retrieve the adequate file from the information provided 
            string[] lines = System.IO.File.ReadAllLines("openvibe_xp_files_and_signals\signals\configS" + subjectID.ToString() + ".txt");
            float median_left = float.Parse(lines[21].Split(':')[1], CultureInfo.InvariantCulture);
            float MAD_left = float.Parse(lines[22].Split(':')[1], CultureInfo.InvariantCulture);
            float median_right = float.Parse(lines[23].Split(':')[1], CultureInfo.InvariantCulture);
            float MAD_right = float.Parse(lines[24].Split(':')[1], CultureInfo.InvariantCulture);

            int colStim = 4;

            //Foreach run
            for (int run = nbR1; run < nbR2 + 1; run++) {
                Console.WriteLine("Run : " + run);

                //init variables
                List<float> LDArightOutputs = new List<float>();
                List<float> LDAleftOutputs = new List<float>();
                line_left = new string[] { };
                line_right = new string[] { };
                trialType = ' ';
                is_perf_to_save = false;
                is_rest = false;
                nb_fb_window = 0;
                nb_window_recognized_left = 0;
                nb_window_recognized_right = 0;
                nb_trial_left = 0;
                nb_trial_right = 0;
                nb_window_rest = 0;
                nb_window_right = 0;
                nb_window_left = 0;

                //Read CSV files
                if (is_concat) { 
                    lines_file_left = System.IO.File.ReadAllLines("openvibe_xp_files_and_signals\signals\XPfeedback_S" + subjectID.ToString() + "_Session" + session.ToString() + "_r" + nbR1.ToString() + "to" + nbR2save.ToString() + "_classifier_output_tonorm_left.csv"); 
                    lines_file_right = System.IO.File.ReadAllLines("openvibe_xp_files_and_signals\signals\XPfeedback_S" + subjectID.ToString() + "_Session" + session.ToString() + "_r" + nbR1.ToString() + "to" + nbR2save.ToString() + "_classifier_output_tonorm_right.csv");
                } else { 
                    
                    lines_file_left = System.IO.File.ReadAllLines("openvibe_xp_files_and_signals\signals\XPfeedback_S" + subjectID.ToString() + "_Session" + session + "_r" + run.ToString() + "_online_classifier_output_left.csv");
                    lines_file_right = System.IO.File.ReadAllLines("openvibe_xp_files_and_signals\signals\XPfeedback_S" + subjectID.ToString() + "_Session" + session + "_r" + run.ToString() + "_online_classifier_output_right.csv");
                }

                //Foreach line in the file
                for (int l = 1; l < lines_file_left.Count(); l++) {
                    line_left = lines_file_left[l].Split(',');
                    line_right = lines_file_right[l].Split(',');
                    last_stimulation = dict[line_left[colStim]];
                    if (last_stimulation == "OVTK_GDF_Up") {
                        is_rest = true;
                    } else if (last_stimulation == "OVTK_StimulationId_BaselineStop") {
                        is_rest = false;
                    } else if (last_stimulation == "OVTK_GDF_Left") {
                        nb_trial_left++;
                        trialType = 'L';
                    } else if (last_stimulation == "OVTK_GDF_Right") {
                        nb_trial_right++;
                        trialType = 'R';
                    } else if (last_stimulation == "OVTK_StimulationId_Target") {
                        is_perf_to_save = true;
                    } else if (last_stimulation == "OVTK_StimulationId_NonTarget") {
                        is_perf_to_save = false;
                    } else if (last_stimulation == "OVTK_GDF_End_Of_Trial") {
                        trialType = ' ';
                    }
                    
                    //If their is a feedback displayed then compute the performances
                    if (is_rest || is_perf_to_save) {
                        LDA1_left = float.Parse(line_left[2], CultureInfo.InvariantCulture);
                        LDA2_left = float.Parse(line_left[3], CultureInfo.InvariantCulture);
                        LDA1_right = float.Parse(line_right[2], CultureInfo.InvariantCulture);
                        LDA2_right = float.Parse(line_right[3], CultureInfo.InvariantCulture);

                        perf_left = (((Math.Abs(LDA2_left) - Math.Abs(LDA1_left)) / (Math.Abs(LDA2_left) + Math.Abs(LDA1_left))) - median_left) / (MAD_left * 2);
                        perf_right = (((Math.Abs(LDA2_right) - Math.Abs(LDA1_right)) / (Math.Abs(LDA2_right) + Math.Abs(LDA1_right))) - median_right) / (MAD_right * 2);

                        if (is_rest) {
                            nb_window_rest++;
                            if (perf_left <= 0)
                                nb_window_recognized_left++;
                            if (perf_right <= 0)
                                nb_window_recognized_right++;
                        }
                        else if (is_perf_to_save & trialType == 'L') {
                            nb_window_left++;
                            if (perf_left >= 0)
                                nb_window_recognized_left++;
                        } 
                        else if (is_perf_to_save & trialType == 'R') {
                            nb_window_right++;
                            if(perf_right >= 0)
                                nb_window_recognized_right++;
                        }
                        nb_fb_window++;
                    }
                }

                //Save the performances in a dedicated .txt file
                using (System.IO.StreamWriter file = new System.IO.StreamWriter("openvibe_xp_files_and_signals\signals\online_performances_S" + subjectID.ToString() + ".txt", true)) {
                    Console.WriteLine("Nb window right : " + (nb_window_right).ToString() + " - Nb window left : " + (nb_window_left).ToString() + "Nb window total : " +  nb_window_rest.ToString());
                    file.WriteLine("Run - " + run.ToString() + " - Mean performance right: " + (nb_window_recognized_right * 100 / (nb_window_right + nb_window_rest)).ToString() + " - Mean performance  left: " + (nb_window_recognized_left * 100 / (nb_window_left + nb_window_rest)).ToString() + " (NB data rest: " + nb_window_rest.ToString() + " NB data left:" + nb_window_left.ToString() + " NB data right: " + nb_window_right.ToString() + ")");
                }
            }
            Console.ReadLine();
        }
    }
}
