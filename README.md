# BCI-GEST

BCI based grasping exercise for user training

There are five sub folders in this one:
   - visualFB: Main Unity program (to run with the OpenViBE scenario "2-acquisition.xml" or "6-online.xml") program that enables the BCI user to have a realistic visual stimulation which speed currently depends on an input provided by OpenViBE (Four int that enable to compute the performances and one int that represents the stimulation).
   - baseline_visualFB: Unity program (to run with the OpenViBE scenario "1-baseline_FBvisuel.xml") that enables the BCI user to have a realistic visual stimulation with all the different hand movement speed.
   - check_intensity_visualFB: Unity program (to run without OpenViBE) that enables to find the adapted parameters of the visual feedback for a participant.
   - openvibe_xp_files_and_signals: Folder that contains all the OpenViBE scenarii and were electrophysiological signals are stored
   - retrieveOnlivePerformances: C# program that enables to compute the performances of a participant.
