Author : léa pillette
Date : 16-03-2019
Purpose : Brain-Computer Interface Grasping Exercise for uSer Training (BCI-GEST) 

This Unity program (to run with the OpenViBE scenario "1-baseline_FBvisuel.xml") enables the BCI user to have a realistic visual stimulation with all the different hand movement speed.