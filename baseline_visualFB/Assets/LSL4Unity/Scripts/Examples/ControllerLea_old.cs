﻿using UnityEngine;
using System;
using System.Linq;
using Assets.LSL4Unity.Scripts.AbstractInlets;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Assets.LSL4Unity.Scripts.Examples
{
    /// <summary>
    /// Just an example implementation for a Inlet recieving float values
    /// </summary>
    public class ControllerLea_old : AFloatInlet
    {
        //Public 
        public GameObject human;
        public GameObject blackScreen;
        public GameObject cross;
        public GameObject leftArrow;
        public GameObject rightArrow;
        public GameObject txtRest;

        //Private
        Animator animHuman;
        private List<string> logs = new List<string>();
        string lastSample = String.Empty;
        private Dictionary<string, string> dictStim;
        private bool ongoingFB;
        private int nbrLslData;
        private string lastStimulation;
        private float lastPerfLeft;
        private float lastPerfRight;
        private char trialType;

        private bool positive_fb;
        private bool biased_fb;
        private float bias;

        private int speed_min_left;
        private int speed_min_right;

        private void Awake() {
            dictStim = CreateDictStim();
            animHuman = human.GetComponent<Animator>();
            ongoingFB = false;
            trialType = ' ';
            
            //Disable the 2D elements
            cross.SetActive(false);
            rightArrow.SetActive(false);
            leftArrow.SetActive(false);
            txtRest.SetActive(false);

            //Get config information
            //string[] lines = System.IO.File.ReadAllLines(@"C:\Users\lpillett\Desktop\Current_work\BCI & Stroke\ProtocoleStroke\openvibe_xp_files_and_signals\configS" + subjectNb.ToString() + "r" + runNb.ToString() + ".txt");

            //positive_fb = (lines[0][lines[0].Length - 1] == 'Y') ? true : false;
            //biased_fb = (lines[2][lines[0].Length - 1] == 'Y') ? true : false;
            //bias = float.Parse(lines[3].Split(':')[1]);

            //speed_min_left = (int)double.Parse(lines[13].Split(':')[1]);
            //speed_min_right = (int)double.Parse(lines[14].Split(':')[1]);
        }

        private void Update() {
            Application.runInBackground = true;
        }

        protected override void Process(float[] newSample, double timeStamp)
        {
            // just as an example, make a string out of all channel values of this sample
            lastSample = string.Join(" ", newSample.Select(c => c.ToString()).ToArray());

            Debug.Log(
                string.Format("Got {0} samples at {1} : {2}", newSample.Length, timeStamp, newSample[0])
                );
            //Save the LSL data received 
            logs.Add(timeStamp.ToString() + ',' + string.Join(",", newSample.Select(c => c.ToString()).ToArray()));

            //String of the last stimulation
            lastStimulation = dictStim[newSample[4].ToString()];

            //If a stimulation was received
            if (lastStimulation != "NoValue")
            {
                Debug.Log(
                        string.Format("Stimulation : {0}", lastStimulation)
                );
                if (lastStimulation == "OVTK_GDF_Start_Of_Trial")
                {
                    cross.SetActive(true);
                }
                else if (lastStimulation == "OVTK_GDF_Left")
                {
                    leftArrow.SetActive(true);
                    trialType = 'L';
                }
                else if (lastStimulation == "OVTK_GDF_Right")
                {
                    rightArrow.SetActive(true);
                    trialType = 'R';
                }
                else if (lastStimulation == "OVTK_GDF_Up") {
                    txtRest.SetActive(true);
                    trialType = 'U';
                }
                else if (lastStimulation == "OVTK_GDF_End_Of_Trial")
                {
                    trialType = ' ';
                    ongoingFB = false;
                    animHuman.SetTrigger("ToRest");
                    animHuman.speed = 1;
                    cross.SetActive(false);
                    rightArrow.SetActive(false);
                    leftArrow.SetActive(false);
                    txtRest.SetActive(false);
                }
                else if (lastStimulation == "OVTK_StimulationId_ExperimentStop")
                {
                    //Set back black screen
                    blackScreen.SetActive(true);
                }
                if (lastStimulation == "OVTK_GDF_Feedback_Continuous")
                {
                    ongoingFB = true;
                    if (trialType == 'R')
                        animHuman.SetTrigger("ToMoveRight");
                    else if (trialType == 'L')
                        animHuman.SetTrigger("ToMoveLeft");
                }
            }

            //If their is a feedback displayed
            if (ongoingFB)
            {
                lastPerfLeft = newSample[0];
                lastPerfRight = newSample[2];
                if (trialType == 'R' & lastPerfRight > 0)
                    animHuman.speed = Math.Abs(lastPerfRight) / (float)0.5;
                else if (trialType == 'L' & lastPerfLeft > 0)
                    animHuman.speed = Math.Abs(lastPerfLeft) / (float)0.5;
                else
                    animHuman.speed = 0;
            }
        }

        void OnApplicationQuit() {
            File.WriteAllLines("logsLSL_processed.txt", logs.ToArray());
        }

        /// <summary>
        /// Contains OpenViBE stimulations
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> CreateDictStim() {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["32769"] = "OVTK_StimulationId_ExperimentStart";
            dict["32770"] = "OVTK_StimulationId_ExperimentStop";
            dict["32775"] = "OVTK_StimulationId_BaselineStart";
            dict["32776"] = "OVTK_StimulationId_BaselineStop";
            dict["33282"] = "OVTK_StimulationId_Beep";
            dict["768"] = "OVTK_GDF_Start_Of_Trial";
            dict["769"] = "OVTK_GDF_Left";
            dict["770"] = "OVTK_GDF_Right";
            dict["780"] = "OVTK_GDF_Up";
            dict["781"] = "OVTK_GDF_Feedback_Continuous";
            dict["786"] = "OVTK_GDF_Cross_On_Screen";
            dict["800"] = "OVTK_GDF_End_Of_Trial";
            dict["0"] = "NoValue";
            return dict;
        }
    }
}